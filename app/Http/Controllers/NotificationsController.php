<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index(Request $request)
    {
        //$unreadNotifications = auth()->user()->unreadNotifications;
        if ($request->ajax()) {
            return auth()->user()->unreadNotifications;
        }

        return view('notifications.index', [
            'unreadNotifications' => auth()->user()->unreadNotifications,
            'readNotifications' => auth()->user()->readNotifications,
        ]);
    }

    public function read(Request $request, $id)
    {
        DatabaseNotification::find($id)->markAsRead();

        if ($request->ajax()) {
            return auth()->user()->unreadNotifications;
        }

        return back()->with('flash', 'La notificación ha sido leída');
    }

    public function destroy($id)
    {
        DatabaseNotification::find($id)->delete();

        return back()->with('flash', 'La notificación ha sido eliminada');
    }
}
