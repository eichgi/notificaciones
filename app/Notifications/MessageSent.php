<?php

namespace App\Notifications;

use App\Message;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MessageSent extends Notification
{
    use Queueable;
    public $message;

    /**
     * Create a new notification instance.
     *
     * @param $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //return ['database'];
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /*return (new MailMessage)
            ->subject('Nuevo mensaje de prueba')
            ->line('Has recibido un mensaje')
            ->action('Click para ver el mensaje', route('messages.show', $this->message->id))
            ->line('Gracias por usar la app');*/

        return (new MailMessage())->view('email.notifications',
            [
                'data' => $this->message,
                'user' => $notifiable,
            ])->subject('Has recibido un nuevo mensaje');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'link' => route('messages.show', $this->message->id),
            'text' => 'Has recibido un nuevo mensaje de ' . $this->message->sender->name,
        ];
    }
}
