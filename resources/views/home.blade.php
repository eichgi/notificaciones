@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Enviar mensaje</div>

                    <div class="card-body">
                        <form action="{{route('messages.store')}}" method="POST">
                            @csrf
                            <div class="form-group {{$errors->has('recipient_id') ? 'has-error' : ''}}">
                                <select name="recipient_id" id="" class="form-control">
                                    <option value="">elige un usuario</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('recipient_id', '<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="form-group {{$errors->has('body') ? 'has-error' : ''}}">
                            <textarea name="body" id="" cols="30" rows="10" class="form-control"
                                      placeholder="Escribe aquí tu mensaje"></textarea>
                                {!! $errors->first('body', '<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-block">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
