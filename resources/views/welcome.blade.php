@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Inicio</div>

                    <div class="card-body">
                        @foreach(App\Post::latest()->get() as $post)
                            <div class="card-header">
                                <a href="{{route('posts.show', $post)}}">{{$post->title}}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
